### timezone-generator
#### Description
A Python script which calls the Google Maps API to fetch time 
zone data for a given JSON input file containing latitude and 
longitude coordinates and outputs the results to an .impex file 
for processing in Hybris.
#### Scripts
This project contains two scripts:

``TimeZoneGenerator.py``

``AsyncTimeZoneGenerator.py``

Both scripts achieve the same functionality. AsyncTimeZoneGenerator.py
is faster, but should only be used when executing against smaller
input files (< 100 entries) due to API restrictions. 
When in doubt, use TimeZoneGenerator.py.

See 'API Request Restrictions' for more details.

#### Dependences
python3.7 >

``brew install python``

For AsyncTimeZoneGenerator.py:

``pip3 install --user asyncio``

``pip3 install --user aiohttp``
#### Input and Execution
Either script requires a JSON input file named 'locations.json' to be present
in the same directory for execution (see '/sample_json' for an example). 
This file can be generated via SQL based on the following query against 
the Hybris database:

``
SELECT p_name, P_description, p_timezone, p_latitude, p_longitude
FROM pointofservice 
WHERE (p_timezone IS NULL) 
AND p_latitude IS NOT NULL 
AND p_longitude IS NOT NULL
``

Once the result set is returned, use your database tool of choice 
(i.e., Azure Data Studio) to export the result set to JSON.
#### Output
Either script will format and output the results of the API calls to a file 
called 'timezone_output.impex':

``INSERT_UPDATE PointOfService;name[unique=true];type(code);timezone``

``;1725300;STORE;America/New_York``

``;1809880;STORE;America/New_York``

``...``

This impex file can be imported via Backoffice to update the stores with their missing 
timezones accordingly.
#### API Request Restrictions
In cases of request denial due to API request restrictions, a throttle argument 
may be passed to TimeZoneGenerator.py to limit how quickly requests are sent to the API:

``python3 TimeZoneGenerator.py --throttle <seconds>``

A recommended throttle of 1 second should be enough to handle most cases.

This is a first pass solution. Updates and suggestions are welcome if this ends up being used often.