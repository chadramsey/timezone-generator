import argparse
import time
import requests
import json
from datetime import datetime

parser = argparse.ArgumentParser()
parser.add_argument("--throttle", "-t", help="Sets a throttle (in seconds) to limit requests to the API")
args = parser.parse_args()


def print_location_data_to_impex(data):
    if args.throttle:
        time.sleep(int(args.throttle))
    if data['p_latitude'] != 0.0 and data['p_longitude'] != 0.0:
        timezone_request = requests.get(url="https://maps.googleapis.com/maps/api/timezone/json",
                                        params={'location': f"{data['p_latitude']},{data['p_longitude']}",
                                                'timestamp': 1458000000,
                                                'key': 'AIzaSyDx06sGfB4by0eKSgxV1gv81Zzp2x0k7wY'})

        timezone_response_json = timezone_request.json()
        if timezone_response_json['status'] != 'ZERO_RESULTS':
            print(f"Writing line for {data['p_name']} -> {timezone_response_json['timeZoneId']}")
            timezone_impex_output_file.write(f";{data['p_name']};STORE;{timezone_response_json['timeZoneId']}\n")


start_time = datetime.now()

timezone_json_input_file = open('locations.json', )
location_data = json.load(timezone_json_input_file)
timezone_json_input_file.close()

timezone_impex_output_file = open('timezone_output.impex', 'w')
timezone_impex_output_file.write("INSERT_UPDATE PointOfService;name[unique=true];type(code);timezone\n")

print(f"Processing {len(location_data)} records")

for i in location_data:
    print_location_data_to_impex(i)

timezone_impex_output_file.close()

print("Writes complete. Output file: timezone_output.impex")
print(f"Operation took: {datetime.now() - start_time} seconds")
