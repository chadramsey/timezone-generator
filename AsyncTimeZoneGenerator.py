import asyncio
import aiohttp
import json
from datetime import datetime


async def fetch(data, session):
    if data['p_latitude'] != 0.0 and data['p_longitude'] != 0.0:
        async with session.get(url="https://maps.googleapis.com/maps/api/timezone/json",
                               params={'location': f"{data['p_latitude']},{data['p_longitude']}",
                                       'timestamp': 1458000000,
                                       'key': 'AIzaSyDx06sGfB4by0eKSgxV1gv81Zzp2x0k7wY'}) as response:
            print(f"Preparing request for: {data['p_name']} | {data['P_description']}")
            json_response = {"name": data['p_name'], "description": data['P_description']}
            web_response = await response.json()
            json_response.update(web_response)
            return json_response


async def print_location_data_to_impex(location_data):
    tasks = []
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        print("Executing asynchronous requests")
        for data in location_data:
            task = asyncio.ensure_future(fetch(data, session))
            tasks.append(task)

        responses = await asyncio.gather(*tasks)

        print("Responses resolved. Writing output to file")

        timezone_impex_output_file = open('timezone_output.impex', 'w')
        timezone_impex_output_file.write("INSERT_UPDATE PointOfService;name[unique=true];type(code);timezone\n")

        for response in responses:
            if response is not None and response['status'] != 'ZERO_RESULTS':
                print(f"Writing line for {response['name']} -> {response['timeZoneId']}")
                timezone_impex_output_file.write(f";{response['name']};STORE;{response['timeZoneId']}\n")

        timezone_impex_output_file.close()


start_time = datetime.now()

timezone_json_input_file = open('locations.json', )
location_data = json.load(timezone_json_input_file)
timezone_json_input_file.close()

print(f"Processing {len(location_data)} records")

loop = asyncio.get_event_loop()
future = asyncio.ensure_future(print_location_data_to_impex(location_data))
loop.run_until_complete(future)

print("Writes complete. Output file: timezone_output.impex")
print(f"Operation took: {datetime.now() - start_time} seconds")
